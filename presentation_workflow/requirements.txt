asgiref==3.6.0
black==23.3.0
certifi==2023.5.7
charset-normalizer==3.1.0
click==8.1.3
Django==4.2.1
idna==3.4
mypy-extensions==1.0.0
packaging==23.1
pathspec==0.11.1
platformdirs==3.5.0
requests==2.30.0
sqlparse==0.4.4
tomli==2.0.1
urllib3==2.0.2
gunicorn ==20.1.0
pika==1.2.0
